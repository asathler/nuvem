# ELEIÇÃO - Projeto de estudo

**Índice**

[TOC]

## Cloud

- [x] AWS   - Free             / exige cartão de crédito
- [ ] GCP   - US$ 300 créditos / exige cartão de crédito
- [ ] Azure - ?
- [ ] OCI   - ?
- [ ] outro - ?

## Arquitetura preliminar

- [ ] PHP 8.2.x
- [ ] Laravel 10.x
- [ ] Postgres 15.3.x
- [ ] Full-text search
- [ ] Queue
- [ ] Jobs?
- [ ] GitLab
- [ ] CI/CD
- [ ] DevOps

## Interface

- [ ] Dashboard (Grafana)
- [ ] Tailwind?
- [ ] Bootstrap?
- [ ] Vue.js?

## Estratégia

- [ ] Back+front?
- [ ] Microserviços?
- [ ] SaaS?
- [ ] Multitenant por empresa/instituição?

## Negócio

- [ ] Eleição
  - [ ] `evento`
    - [ ] `histórico`
    - [ ] `pré-requistos para abertura`
    - [ ] `regulamento / regimento`
  - [ ] `chapas`
  - [ ] `candidatos`
    - [ ] `documentos (pdf)` *Cloud?*
  - [ ] `eleitores`
    - [ ] `documentos (pdf)` *Cloud?*
  - [ ] `geo (brasil, região, ufs)`
  - [ ] `votação`
    - [ ] `segurança!`
  - [ ] `apuração`
  - [ ] `resultados`

## Exemplo

### Instituição ABC

- evento: Eleição gestão 2023-25
- chapas 1 e 2
- candidatos a: Presidente, Vice e Tesoureiro
- eleitores: 2 por estado, +6 extras = 60
- votação: 10min (teste)
- apuração:
- resultados:
